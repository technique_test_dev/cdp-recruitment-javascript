const { main } = require('../app');
const chai = require('chai');
const { dataCountable, dataCountries, dataFiltered, dataFilterCount, dataCountFilter } = require('./mocks');

const expect = chai.expect

describe('App test', () => {

  describe('with index entry', () => {

    it('should have no argument', () => {
      expect(main()).to.be.equal('Nothing to do')
    })

    it('should not contains argument', () => {
      const arg = null
      const result = main(arg)

      expect(result).is.string
      expect(result).to.be.equal("Nothing to do")
    })

    it('should contains empty argument', () => {
      const arg = {
        argv: Array(2)
      }

      const result = main(arg)
      
      expect(result).is.string
      expect(result).to.be.equal("Argument is empty")
    })

    it('should contains unrecognized argument', () => {
      const arg = {
        argv: ['','',' ']
      }
      const result = main(arg)

      expect(result).is.string
      expect(result).to.deep.include.members(dataCountries)
    })

    it('should contains argument recognize --count', () => {
      const arg = {
        argv: ['','','--count']
      }
      const result = main(arg)

      expect(result).is.string
      expect(result).to.deep.include.members(dataCountable.data)
    })

    it('should contains argument recognize --filter=ouse', () => {
      const arg = {
        argv: ['','','--filter=ouse']
      }
      const result = main(arg)

      expect(result).is.string
      expect(result).to.deep.include.members(dataFiltered.data)
    })

    it('should contains argument --count and --filter=ry', () => {
      const arg = {
        argv: ['','','--count','--filter=ry']
      }
      const result = main(arg)

      expect(result).is.string
      expect(result).to.deep.include.members(dataCountFilter)
    })

    it('should contains argument --filter=ouse and --count', () => {
      const arg = {
        argv: ['','','--filter=ouse','--count']
      }
      const result = main(arg)

      expect(result).is.string
      expect(result).to.deep.include.members(dataFilterCount)
    })

  })

})
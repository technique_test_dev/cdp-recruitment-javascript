const countries = require("../data/data").data;

module.exports = {
  dataCountries: countries,

  dataCountPeoples: {
    emptyData: [],
    data:[
      {
        name: 'Elmer Kinoshita [7]',
        animals: [
          { name: 'Weasel' },
          { name: 'Birds' },
          { name: 'Snakes' },
          { name: 'Anteater' },
          { name: 'Groundhog' },
          { name: 'Ant' },
          { name: 'Courser' }
        ]
      }
    ]
  },

  dataCountable: {
    emptyData: [],
    data:[
      {
        name: "Dillauti [5]",
        people: [
          {
            name: "Winifred Graham [6]",
            animals: [
              {
                name: "Anoa"
              },
              {
                name: "Duck"
              },
              {
                name: "Narwhal"
              },
              {
                name: "Badger"
              },
              {
                name: "Cobra"
              },
              {
                name: "Crow"
              }
            ]
          },
          {
            name: "Blanche Viciani [8]",
            animals: [
              {
                name: "Barbet"
              },
              {
                name: "Rhea"
              },
              {
                name: "Snakes"
              },
              {
                name: "Antelope"
              },
              {
                name: "Echidna"
              },
              {
                name: "Crow"
              },
              {
                name: "Guinea Fowl"
              },
              {
                name: "Deer Mouse"
              }
            ]
          },
          {
            name: "Philip Murray [7]",
            animals: [
              {
                name: "Sand Dollar"
              },
              {
                name: "Buzzard"
              },
              {
                name: "Elephant"
              },
              {
                name: "Xenops"
              },
              {
                name: "Dormouse"
              },
              {
                name: "Anchovy"
              },
              {
                name: "Dinosaur"
              }
            ]
          },
          {
            name: "Bobby Ristori [9]",
            animals: [
              {
                name: "Kowari"
              },
              {
                name: "Caecilian"
              },
              {
                name: "Common Genet"
              },
              {
                name: "Chipmunk"
              },
              {
                name: "Aardwolf"
              },
              {
                name: "Przewalski's Horse"
              },
              {
                name: "Badger"
              },
              {
                name: "Sand Cat"
              },
              {
                name: "Linne's Two-toed Sloth"
              }
            ]
          },
          {
            name: "Louise Pinzauti [5]",
            animals: [
              {
                name: "Manta Ray"
              },
              {
                name: "Nubian Ibex"
              },
              {
                name: "Warbler"
              },
              {
                name: "Duck"
              },
              {
                name: "Mice"
              }
            ]
          }
        ]
      },
    ]
  },

  dataFilterPeople:{
    emptyData: [],
    data:[
      {
        name: 'Winifred Graham',
        animals:
          [
            {name: 'Badger'}
          ]
      }
    ]
  },

  dataFiltered:{
    emptyData: [],
    data:[
      {
        name: "Dillauti",
        people: [
          {
            name: "Blanche Viciani",
            animals: [
              {
                name: "Deer Mouse"
              }
            ]
          },
          {
            name: "Philip Murray",
            animals: [
              {
                name: "Dormouse"
              }
            ]
          }
        ]
      },
      {
        name: "Tohabdal",
        people: [
          {
            name: "Owen Bongini",
            animals: [
              {
                name: "Mouse"
              }
            ]
          }
        ]
      },
      {
        name: "Satanwi",
        people: [
          {
            name: "Dennis Franci",
            animals: [
              {
                name: "Grouse"
              }
            ]
          }
        ]
      }
    ]
  },

  dataCountFilter:[
    {
      name: "Uzuzozne [7]",
      people: [
        {
          name: "Lillie Abbott [6]",
          animals: [
            {
              name: "John Dory"
            }
          ]
        }
      ]
    },
    {
      name: "Satanwi [5]",
      people: [
        {
          name: "Anthony Bruno [6]",
          animals: [
            {
              name: "Oryx"
            }
          ]
        }
      ]
    }
  ],

  dataFilterCount:[
    {
      name: "Dillauti [2]",
      people: [
        {
          name: "Blanche Viciani [1]",
          animals: [
            {
              name: "Deer Mouse"
            }
          ]
        },
        {
          name: "Philip Murray [1]",
          animals: [
            {
              name: "Dormouse"
            }
          ]
        }
      ]
    },
    {
      name: "Tohabdal [1]",
      people: [
        {
          name: "Owen Bongini [1]",
          animals: [
            {
              name: "Mouse"
            }
          ]
        }
      ]
    },
    {
      name: "Satanwi [1]",
      people: [
        {
          name: "Dennis Franci [1]",
          animals: [
            {
              name: "Grouse"
            }
          ]
        }
      ]
    }
  ]

}
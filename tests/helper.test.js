const { countable, filterAnimalByWord, filterAnimals, filterPeople, countPeoples } = require('../helpers');
const chai = require('chai')

const { dataCountPeoples, dataCountries, dataCountable, dataFilterPeople, dataFiltered } = require('./mocks')

const expect = chai.expect

describe('Helper test', () => {

  describe('countable', () => {

    describe('countPeoples', () => {

      it('should have empty data', () => {

        const result = countPeoples([])

        expect(result).to.eql(dataCountPeoples.emptyData)

      })

      it('should have data and return array with data', () => {

        const data = dataCountPeoples.data

        const result = countPeoples(dataCountries[4].people)
        
        expect(result).is.not.empty
        expect(result).to.deep.include.members(data)

      })

    })

    describe('countable', () => {

      it('should have empty data', () => {

        const result = countable([])

        expect(result).to.eql(dataCountable.emptyData)

      })

      it('should have data and return array with data', () => {

        const data = dataCountable.data

        const result = countable(dataCountries)
        
        expect(result).is.not.empty
        expect(result).to.deep.include.members(data)

      })
    })

  })

  describe('Filtered', () => {

    describe('filterAnimals', () => {

      it('shoud have empty array', () => {
        
        const result = filterAnimals([], "ry")

        expect(result).to.eql(dataFilterPeople.emptyData)

      })

      it('shoud have empty value', () => {
        
        const result = filterAnimals(dataCountries[0].people[0].animals, "")

        expect(result).to.be.eql(dataCountries[0].people[0].animals)

      })

      it('shoud have value', () => {
        
        const result = filterAnimals(dataCountries[0].people[0].animals, "w")

        expect(result.length).to.be.equal(2)

      })

    })

    describe('filterPeople', () => {

      it('shoud have empty array', () => {
        
        const result = filterPeople([], "ry")

        expect(result).to.eql(dataFilterPeople.emptyData)

      })

      it('shoud have empty value', () => {
        
        const result = filterPeople(dataCountries[0].people, "")

        expect(result).to.be.eql(dataCountries[0].people)

      })

      it('shoud have value', () => {
        
        const result = filterPeople(dataCountries[0].people, "ad")

        expect(result).to.deep.include.members(dataFilterPeople.data)

      })

    })

    describe('filterAnimalByWord', () => {

      it('shoud have empty array', () => {
        
        const result = filterAnimalByWord([], "ry")
        
        expect(result).to.eql(dataFiltered.emptyData)

      })

      it('shoud have empty value', () => {
        
        const result = filterAnimalByWord(dataCountries, "")

        expect(result).to.be.eql(dataCountries)
        expect(result).to.deep.include.members(dataCountries)

      })

      it('shoud have value', () => {
        
        const result = filterAnimalByWord(dataCountries, "ouse")

        expect(result).to.deep.include.members(dataFiltered.data)

      })

    })

  })

})
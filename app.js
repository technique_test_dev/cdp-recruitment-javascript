const { filterAnimalByWord, countable } = require('./helpers')

let countries = require('./data/data').data

const display = (data) =>{
  console.log(JSON.stringify(data, null, 2))
}

const main = (process) => {

  if (!process || !process.argv)
    return 'Nothing to do'

  const args = process.argv.slice(2)
  
  if(!args || args.length<1)
    return "Argument is empty"
  
  let data = [...countries]

  args.forEach(cmd=>{

    let [key, param] = cmd.split('=')
    
    switch (key) {
      
      case '--filter':
        data = filterAnimalByWord(data, param)
      break;

      case '--count':
        data = countable(data)
      break
    }
    
  })

  return data

}

display(
  main(process)
)

module.exports = {
  main
}
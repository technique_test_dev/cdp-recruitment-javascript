
const filterAnimals = (animals, word="") => animals.filter(({name}) => name.toLowerCase().includes(word.toLowerCase()))

const filterPeople = (peoples, word) => {
  let output = []

  peoples.forEach(({name, animals}) => {
    let animalFiltered = filterAnimals(animals, word)

    if(animalFiltered.length < 1) return;

    output.push({
      name,
      animals: animalFiltered
    })

  })

  return output

}

const filterAnimalByWord = (countries, word="") => {
  let output = []

  countries.forEach(({name, people}) => {
    let peopleFiltered = filterPeople(people, word)

    if(peopleFiltered.length < 1) return;

    output.push({
      name,
      people: peopleFiltered
    })

  })

  return output

}
module.exports = {
  filterAnimalByWord,
  filterAnimals,
  filterPeople,
}
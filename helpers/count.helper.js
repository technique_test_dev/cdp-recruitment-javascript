const countPeoples = (peoples) => {
  return peoples.map(({name, animals})=>{
    return {
      name: name.concat(' [', animals.length, ']'),
      animals
    }
  })
}

const countable = (countries) => {

  return countries.map(({name, people})=>{
    return {
      name: name.concat(' [', people.length, ']'),
      people: countPeoples(people)
    }
  })
}

module.exports = {
  countable,
  countPeoples,
}